/**
 * Created by Tomasz Dziuba on 30.12.2016.
 */

import Header from './components/Header';
import GoUpLink from './components/GoUpLink';
import { GeocoderHelper, Translator, AdLoader } from './helpers';
import './components/StickyAd';
import BootstrapLoader from './vendor/bootstrap';

window.cbApp = window.cbApp || {};

export function executeCommon() {
	new GoUpLink();

	window.cbApp.GeocoderHelper = new GeocoderHelper();
	// window.cbApp.ServerData = new ServerData();
	// window.cbApp.TranslateKeyContainer = TranslateKeyContainer;

	window.cbApp.Header = new Header();
	window.cbApp.Translator = new Translator();

	window.cbApp.Translator.parse();

	window.cbApp.getScrollbarWidth = () => {
		let fakeDiv = $('<div class="scrollbar-fake-div">').appendTo('body'),
			maxW = $('<div style="width: 100%">').appendTo(fakeDiv).outerWidth();

		fakeDiv.remove();

		return 100 - maxW;
	};

	new BootstrapLoader();

	/**
	 * ad parsing and executing code
	 */
	AdLoader();

	$('#id_103').stickyAd({bottomEl: '#tagCloud'});
	$('#id_104').stickyAd({bottomEl: '#tagCloud'});
	$('.tabl-stck').stickyAd({bottomEl: '#tagCloud'});

}

