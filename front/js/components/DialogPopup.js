/**
 * Created by Tomasz Dziuba on 06.02.2017.
 */
import ModalLayer from './ModalLayer';

export default class DialogPopup extends ModalLayer {

	onSaveCallback;

	constructor (options) {
		super(options);

		this.o = $.extend({}, {
			showFooter			: true,
			showHeader			: true,
			title				: 'System message',
			cancelActionLabel	: null,
			proceedActionlabel	: 'Ok'
		}, parent.o);

		this.handleEvents();
	}

	handleEvents () {
		let self = this;

		$('#' + this.o.id).on('click', '.proceedBtn', (ev) => {
			ev.preventDefault();

			self.onSuccess();
		});
	}

	setSuccessCallback (callback) {
		this.onSaveCallback = callback;
	}

	onSuccess () {
		this.getModal().modal('hide');
		this.onSaveCallback();
	}
}
