/**
 * Created by Tomasz Dziuba on 29.12.2016.
 */
const FB = FB || window.FB;
let cbApp = cbApp || window.cbApp;

export default class FBHelper {
	constructor(props) {
		this.props = props;

		this.init();
	}

	init() {
		window.fbAsyncInit = function() {
			if (typeof FB !== 'undefined') {
				FB.init({
					appId: (typeof cbApp.fb_appId !== 'undefined') ? cbApp.fb_appId : 401652553214093,
					xfbml: true,
					version: 'v2.8'
				});
				FB.AppEvents.logPageView();
			}
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		return this;
	}

	share(callback) {
		if (typeof FB !== 'undefined') {
			FB.ui(
				{
					method: 'share',
					href: document.location.href
				}, function (response) {
					console.log(response);
					callback();
				});
		}
	}
}
