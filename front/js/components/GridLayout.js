/**
 * Created by Tomasz Dziuba on 18.01.2017.
 */

import Masonry from '../vendor/masonry.pkgd';
import imagesLoaded from '../vendor/imagesloaded.pkgd';
import LoadMore from './LoadMore';
import ShopelloItems from './ShopelloItems';

const options = {
	itemSelector: '.js_board_item',
	percentPosition: true,
	columnWidth: '.js_board_item',
	isAnimated: false,
	gutter: 0,
	transitionDuration: 0,
	initLayout: true,
	stamp: '#cbSidebar'
};

export default class GridLayout {
	bricks;
	container;
	items;

	constructor () {
		this.container = $('#cbContent').find('.masonry-layout');
		this.items = this.container[0].querySelectorAll('.gridItem');
		this.recalculate();

		this.bricks = new Masonry(this.container[0], options);

		this.handleEvents();

		this.getBricks().layout();

		this.loadMore = new LoadMore({oGrid: this.container, oParent: this});
		this.shopelloItems = new ShopelloItems({parent: this});
	}

	getBricks () {
		return this.bricks;
	}

	handleEvents () {
		let self = this,
			imgL;

		this.getBricks().once('layoutComplete', () => {
			self.container.parent().removeClass('processing');
		});

		imgL = imagesLoaded( self.container[0], () => {
			self.getBricks().layout();
		});

		$(window).on('resize', () => {
			self.recalculate();
		});

		$(window).on('orientationchange', () => {
			self.recalculate();
		});

		imgL.on('progress', ( instance, image ) => {
			window.calculateImgHeight($(image.img).parents('.gridItem')[0]);
		});

	}

	recalculate (items) {
		let self = this,
			t;

		if (typeof t !== 'undefined') clearTimeout(t);
		t = setTimeout(() => {
			window.calculatePlaceholders(self.items, self.container[0]);
		}, 250);
	}

}

$(document).ready(() => {
	window.cbApp.GridLayout = new GridLayout();
});
