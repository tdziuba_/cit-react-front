/**
 * Created by Tomasz Dziuba on 30.12.2016.
 */
import Hammer from '../vendor/hammer';

export default class Header {
	props;

	constructor (props) {
		let options = {
			clearBtn: $('#unsetPhrase'),
			searchInput: $('#searchInput'),
			showSearchBtn: $('#showSearchFormBtn'),
			menu: $('#sideMenu'),
			layer: $('#sideMenuLayer'),
			menuBtn: $('#showSideMenuBtn'),
			formWrp: $('#searchFormWrp'),
			form: $('#searchForm')
		};

		this.props = $.extend({}, options, props);

		this.init();
	}

	init () {
		let self = this;

		try {

			this.props.searchInput.on('keyup', () => {
				self.handleSearchInput();
			});

			this.props.clearBtn.on('click', () => {
				self.props.clearBtn.addClass('invisible');
				self.props.searchInput.val('');
			});

			this.props.showSearchBtn.on('click', () => {
				self.handleMobileSearchForm();
			});

			this.handleSideMenu();

		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	}

	handleSearchInput () {

		if (this.props.searchInput.val().trim() !== '') {
			this.props.clearBtn.removeClass('invisible');
		} else {
			this.props.clearBtn.addClass('invisible');
		}
	}

	handleMobileSearchForm () {

		this.props.showSearchBtn.toggleClass('active');

		this.props.formWrp.toggleClass('hidden-md-down');
	}

	handleSideMenu () {
		let self = this,
			hm = new Hammer(this.props.menu[0]),
			hmL = new Hammer(this.props.layer[0]);

		this.props.menuBtn.on('click', () => {
			self.openMenu();
		});

		this.props.menu.on('click', '#sideMenuCloseBtn', () => {
			self.hideMenu();
		});

		this.props.layer.off('click').on('click', () => {
			self.hideMenu();
		});

		hm.on('swipeleft', () => {
			self.hideMenu();
		});

		hmL.on('swipeleft', () => {
			self.hideMenu();
		});
	}

	openMenu () {
		let scrollbarWidth = window.cbApp.getScrollbarWidth();
		this.props.menu.addClass('open');
		$('#bodyId').addClass('no-scroll').css('padding-right', scrollbarWidth);
		$('#cbHeader').css('right', scrollbarWidth);
	}

	hideMenu () {
		this.props.menu.removeClass('open');
		$('#bodyId').removeClass('no-scroll').removeAttr('style');
		$('#cbHeader').removeAttr('style');
	}
}
