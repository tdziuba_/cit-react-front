/**
 * Created by Tomasz Dziuba on 04.01.2017.
 */

import Handlebars from 'handlebars';
import ErrorLogger from '../helpers/ErrorLogger';

export default class LoadMore {
	o = {};
	offers = [];
	total = 0;
	allowNextRequest = true;

	constructor (options) {
		this.o = $.extend({}, {
			oBtn: $('#loadMoreOffersBtn'),
			oGrid: $('.masonry-layout'),
			oParent: cbApp.GridLayout,
			tpl: '',
			offset: 60,
			counter: 60,
			location: null,
			category: null,
			searchText: null,
			pages: 0,
			currentPage: null,
			initiated: false,
			maxPages: 10, //how much pages with items are allowed before starting removing
			removingPages: true //remove or leave previously viewed page when maxPages are reached
		}, options);

		this.init();
	}

	init () {
		let self = this,
			addOfferBtn = $('#addOfferBtnFooter'),
			goUpBtn = $('#gouplink'),
			t;

		if (!this.o.initiated) {

			this.o.tpl = this.getTemplate();

			this.o.initiated = true;

			/**
			 * click load more button and appending more items
			 */
			this.o.oBtn.on('click', function () {
				let btn = $(this);

				self.o.category = btn.data('category');
				self.o.location = btn.data('location');
				self.o.searchText = btn.data('search-text');

				if (!btn.hasClass('processing')) {
					btn.addClass('processing');

					self.appendOffers();
				}

			});

			$(window).on('scroll', function () {

				if (!addOfferBtn.is(':disabled')) {
					addOfferBtn.attr('disabled', 'disabled').addClass('disabled')
				}
				if (!goUpBtn.is(':disabled')) {
					goUpBtn.attr('disabled', 'disabled').addClass('disabled')
				}

				if (typeof t !== 'undefined') {
					clearTimeout(t);
				}

				t = setTimeout(function () {
					addOfferBtn.removeAttr('disabled').removeClass('disabled');
					goUpBtn.removeAttr('disabled').removeClass('disabled');

					self.handleScroll();
				}, 100);

			});


		}

	}

	/**
	 * returns handlebars template
	 * @returns {null}
	 */
	getTemplate () {
		let tpl = $('#gridItemTemplate').html();

		return (tpl) ? Handlebars.compile(tpl) : null;
	}

	/**
	 * load more offers via ajax
	 */
	loadMoreAction () {
		let self = this,
			data = {
				offset: self.o.offset,
				inp_category_id: self.o.category,
				inp_location_id: self.o.location,
				inp_text: self.o.searchText
			};

		if (this.allowNextRequest) {

			$.ajax({
				url: '/load-more',
				method: 'GET',
				data: data,
				dataType: 'json'
			})
				.done( (resp) => {
					self.o.offset += 30;

					self.allowNextRequest = false;
					self.total = resp.data.totalCount;

					if (resp.status == 'success' && resp.data.offers.length) {
						self.storeNextOffers(resp.data.offers);
					} else {
						self.o.oBtn.remove();
					}

				})
				.fail( () => {
					new ErrorLogger('request to /load-more failed on parameters: ' + JSON.stringify(data));
				});

		}

	}

	/**
	 * append offers to grid
	 */
	appendOffers () {
		var html = '',
			items = [],
			data = this.offers,
			msnry,
			frg = document.createDocumentFragment();

		if (data && data.length) {
			this.pages++;

			msnry = this.o.oParent.getBricks();

			for (var i = 0; i < data.length; ++i) {
				if (data[i].price == '0 DH') {
					data[i].price = 0;
				}

				if (i == data.length - 1) {
					data[i].isLast = true;
				}

				html += this.o.tpl(data[i]);
			}

			$(html).appendTo(frg);
			items = frg.querySelectorAll('.gridItem');

			this.o.oGrid.append($(frg));
			msnry.appended(items);

			for (let i = 0; i < items.length; ++i) {
				window.calculateImgHeight(items[i]);
			}

			if ((this.total - this.o.offset) <= 0) {
				this.o.oBtn.removeClass('processing').addClass('hidden');
			} else {
				this.o.oBtn.removeClass('processing');
			}

			msnry.layout();

			this.clearNextOffers();

		}
	}

	/**
	 * hiding and showing 'item pages' with offers on scroll
	 */
	handleScroll () {
		var self = this,
			toBeRemoved;

		this.prepareNextContent();

		if (this.pages > 2) {

			for (var i = 0; i < this.pages; ++i) {
				var el = $('#itemsPage-' + i);

				if (el.length) {
					if (cbApp.utils.isElementOnScreen(el)) {
						self.showPage(el);
					} else {
						self.hidePage(el);
					}
				}
			}

			// removing pages if there are much more results (more than 600)
			if (this.options.removingPages && this.pages > this.options.maxPages) {
				toBeRemoved = this.pages.splice(0, this.options.maxPages - 1);

				for (i = 0; i < toBeRemoved.length; ++i) {
					//replacing by empty div with equal height
					var elem = $(toBeRemoved[i]);

					elem.before('<div style="height: ' + $(toBeRemoved[i]).height() + '"></div>');
					elem.remove();
				}
			}

		}

	}

	/**
	 * hiding 'out of screen' page
	 * @param el
	 */
	hidePage (el) {
		$(el).addClass('invisible');
	}

	/**
	 * showing page which is 'on screen'
	 * @param el
	 */
	showPage (el) {
		this.currentPage = el;
		$(el).removeClass('invisible');
	}

	/**
	 * do a request for more offers before onclick action and prepare content to eliminate latency
	 * when user is clicking the button and waiting for response
	 */
	prepareNextContent () {
		if (this.allowNextRequest) {
			this.loadMoreAction();
		}
	}

	/**
	 * store temporaly offers to append after button click
	 * @param offers
	 */
	storeNextOffers (offers) {
		this.offers = offers;
	}

	/**
	 * clear offers array
	 */
	clearNextOffers () {
		this.offers = [];

		this.allowNextRequest = (this.total - this.o.offset > 0);
	}

}
