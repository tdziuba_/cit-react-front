/**
 * Created by Tomasz Dziuba on 30.01.2017.
 */
import handlebars from 'handlebars';
import Observer from '../helpers/Observer';

export default class ModalLayer {

	constructor (options) {
		this.tpl = handlebars.compile( $('#modalTpl').html() );
		this.o = $.extend({
			id: '',
			content: '',
			showCloseBtn: true,
			modalOptions: {
				backdrop: true,
				keyboard: true,
				focus: true,
				show: false
			}
		}, options);

		if (this.o.id === '') {
			this.o.id = this.getRandomId();
		}

		this.observer = new Observer();

		let self = this;

		this.generateModal();

		if (this.modal && this.modal.length) {
			this.modal.on('show.bs.modal', () => {
				$('#cbHeader').css('right', cbApp.getScrollbarWidth());
			});

			this.modal.on('shown.bs.modal', () => {
				self.observer.notify('open');
			});

			this.modal.on('hidden.bs.modal', () => {
				self.observer.notify('hidden');
				$('#cbHeader').removeAttr('style');
			});
		}

	}

	getRandomId () {
		let timestamp = new Date();
		return 'modalId_' + timestamp;
	}

	setContent (content) {
		this.o.content = content;

		if (this.modal && this.modal.length) {
			this.modal.find('.modal-content').html(content);
		} else {
			this.generateModal();
			this.modal.find('.modal-content').html(content);
		}

	}

	generateModal () {

		if ( !$('#' + this.o.id).length ) {
			$('body').append(this.tpl(this.o));

			this.modal = $('#' + this.o.id).modal(this.o.modalOptions);
		}

		return this;

	}

	destroyModal () {
		$('#' + this.o.id).remove();
		this.observer.notify('destroyed');
	}

	getModal() {
		if (!this.modal) {
			this.modal = $('#' + this.o.id).modal();
		}

		return this.modal;
	}

	addListener (listener) {
		this.observer.add(listener);
	}

}
