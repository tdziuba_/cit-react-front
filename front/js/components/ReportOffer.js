/**
 * Created by Tomasz Dziuba on 06.02.2017.
 */

import ModalLayer from './ModalLayer';
import handlebars from 'handlebars';
import DialogPopup from './DialogPopup';

export default class ReportOffer {

	button;
	offerId;
	form;

	constructor (options) {
		this.o = $.extend({}, {
			tplSelector		: '#reportOfferTemplate',
			formSelector	: '#reportOfferBox',
			buttonSelector	: '#reportOffer'
		}, options);

		this.tpl = this.getTemplate();
		this.button = $(this.o.buttonSelector);
		this.offerId = this.button.data('id');
		this.layer = new ModalLayer({
			id: 'reportOfferPopup'
		});

		this.layer.setContent(this.tpl({ id: this.offerId }));

		this.handleEvents();
	}

	getTemplate () {
		if (!this.tpl) {
			let source = $(this.o.tplSelector).html();

			if (source) {
				return handlebars.compile(source);
			}

		} else {
			return this.tpl;
		}

		return;
	}

	handleEvents () {
		let self = this;
		this.form = $(this.o.formSelector);

		this.button.on('click', () => {
			self.layer.getModal().modal('show');
		});

		this.form.on('click', '.sendReportBtn', (ev) => {
			ev.preventDefault();

			self.validateForm();
		});

	}

	validateForm () {
		let radio = $('.reportTypeInput:checked', this.form);

		if (radio.length) {
			this.submitForm();
		} else {
			this.showError();
		}

	}

	showError () {
		let errorDialog = new DialogPopup({
			title: 'Form has errors',
			proceedActionlabel: 'Ok'
		});

		errorDialog.setContent('<p>Markera anledningen</p>');
		errorDialog.setSuccessCallback(errorDialog.getModal().modal('hide'));

	}

	submitForm () {

	}
}
 