/**
 * Created by Tomasz Dziuba on 20.01.2017.
 */
import Handlebars from 'handlebars';
import { ErrorLogger } from '../helpers';

export default class ShopelloItems {
	constructor (options) {
		this.o = $.extend({
			masonry: $('#cbContent').find('.masonry-layout'),
			shopelloTplItem: $('#shopelloItemTemplate'),
			parent: (typeof window.cbApp.GridLayout !== 'undefined') ? window.cbApp.GridLayout : null,

			/**
			 * template for shopello offer in #shopelloItemTemplate handlebars template located in list.phtml
			 * @type {null}
			 */

			category: null,

			/**
			 * array of indexes (positions) on which should be shopello item generated
			 * @type {number[]}
			 */
			indexes: [2,12,21,33,44,52]
		}, options);

		this.tpl = (this.o.shopelloTplItem.length) ? this.o.shopelloTplItem.html() : null;
		this.compiled = (this.tpl && Handlebars) ? Handlebars.compile(this.tpl) : null;
		this.category = (this.o.shopelloTplItem.length) ? this.o.shopelloTplItem.data('category') : this.o.category;

		this.loadData();
	}

	loadData () {
		let self = this;

		$.ajax({
			url: '/board/shopello-list-offers',
			dataType: 'json',
			cached: true,
			data: {
				category: self.category,
				search_string: self.getParameterByName('inp_text')
			},
			success: self.onSuccess.bind(self),
			error: self.onError.bind(self)
		})
	}

	getParameterByName (name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';

		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	onSuccess (resp) {
		let self = this;

		try {

			if (resp.data && resp.data.length) {

				if (self.compiled) {

					for (let i in resp.data) {
						self.generateItem(self.compiled, resp.data[i], self.o.indexes[i], resp.data.length, i);
					}

				}
			}

		} catch (err) {
			if (typeof ErrorLogger !== 'undefined') {
				new ErrorLogger(err);
			}
		}
	}

	onError (xhr, ajaxOptions, thrownError) {
		new ErrorLogger(xhr);
	}

	/**
	 * compile shopello offer from template and item data and insert to grid list on index position
	 *
	 * @param template
	 * @param item
	 * @param index
	 */
	generateItem (template, item, index, items_length, i) {
		let self = this,
			msnry = this.o.parent.getBricks();

		if (this.o.parent.container.length) {

			let elem = $(template(item)),
				prevItem = this.o.parent.container.find('.js_board_item').eq(index - 1);

			let img = new Image();

			img.onload = function () {

				if (prevItem.length) {
					prevItem.before( elem );

				} else {
					self.o.parent.container.append( elem );
				}

				try {
					msnry.reloadItems();
					msnry.layout();
				} catch (err) {
					if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
				}

			}
			img.src = item.picture;

		}
	}
}
