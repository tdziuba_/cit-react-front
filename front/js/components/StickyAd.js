/**
 * Created by Tomasz Dziuba on 20.01.2017.
 */

/***
 * StickyAd
 */
(function ( $ ) {

	$.fn.stickyAd = function(options) {
		var self = this,
			bottomAd = (typeof options !== 'undefined' && options.hasOwnProperty('bottomEl') && $(options.bottomEl).length) ? $(options.bottomEl) : $('.cntGroup_bottom'),
			adPositions = [];

		/**
		 * inicjuje nasłuchiwanie zdarzeń i kalkulację pozycji
		 * @param ad jquery DOM object
		 * @return undefined
		 */
		this.init = function (ad, i) {
			var t, MutationObserver, observer;

			$('<div id="stckWrp_' + i +'" class="stckWrp"></div>').insertAfter();

			try {

				MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

				observer = new MutationObserver(function(mutations) {
					mutations.forEach(function(mutation) {
						if (mutation.type == 'childList') {
							adPositions[i] = self.getBasePosition(ad);
						}
					});
				});

				observer.observe(ad[0], {
					subtree: true,
					childList: true,
					attributes: false
				});

			} catch (e) {
				if (typeof console !== 'undefined' && console.log !== 'undefined') {
					console.log("Browser doesn't support Mutation Events");
					console.log(e);
				}
			}
			adPositions[i] = self.getBasePosition(ad);
			self.handleAd(ad, i);

			$(window).scroll(function () {
				self.handleAd(ad, i);
			});

			$(window).on('resize', function () {
				if (typeof t !== 'undefined') clearTimeout(t);

				t = setTimeout(function () {
					adPositions[i] = self.getBasePosition(ad);
					self.handleAd(ad, i);
				}, 200);

			});
		},

			/**
			 *
			 * @param ad - jquery DOM element
			 * @param basePosition - top position of ad before scrolling
			 */
			this.handleAd = function (ad, i) {
				var scroll = $(window).scrollTop(),
					stckWrp = ad.parents('.opxBox > div');

				if ( scroll > adPositions[i].bottom) {

					ad.addClass('isSticky');
					ad.css({'left': adPositions[i].left});
					stckWrp.addClass('.stckWrp');
					stckWrp.css({height: ad.outerHeight(true)});

					if (bottomAd.length && parseInt(ad.offset().top + ad.outerHeight(true)) >= bottomAd.offset().top) {
						ad.removeClass('isSticky');
						stckWrp.removeAttr('style');
					}

				} else {
					ad.removeClass('isSticky');
				}

			};

		this.getBasePosition = function (ad) {

			var parentRect = ad.parent()[0].getBoundingClientRect(),
				body = document.body,
				el = document.documentElement,
				scroll = window.pageYOffset || el.scrollTop || body.scrollTop,
				clientTop = body.clientTop || el.clientTop || 0;


			ad.parents('.opxBox > div').css({height: ad.outerHeight(true)});

			return {top: parentRect.top, bottom: parentRect.top + scroll - clientTop, left: parentRect.left, right: parentRect.right};

		};

		return this.each(function(i) {

			self.init( $(this), i );

		});

	};

}( jQuery ));