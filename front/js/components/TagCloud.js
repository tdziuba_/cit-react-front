/**
 * Created by Tomasz Dziuba on 05.01.2017.
 */

export default class TagCloud {
	o = {};
	oBtn;
	oCloud;
	oParent;

	/**
	 * @param options
	 */
	constructor (options) {

		this.o = $.extend({}, {
			cloudSelector		: '#tagCloud',
			parentSelector		: '#cbContent .cb-board',
			btnSelector			: '#tagCloudBtn',
			btnCloseClass		: 'hideIt',
			cloudOpenClass		: 'open'
		}, options);

		this.oBtn 		= $(this.o.btnSelector);
		this.oCloud 	= $(this.o.cloudSelector);
		this.oParent 	= $(this.o.parentSelector);

		this.init();
	}

	init () {
		let self = this,
			t;

		this.calculatePosition();
		this.handleButtonEvents();

		$(window).on('resize', function () {
			if (typeof t !== 'undefined') {
				clearTimeout(t);
			}

			t = setTimeout(function () {
				self.calculatePosition();
			}, 100);

		});
	}

	/**
	 * set up padding for parent element to fit cloud element at the bottom
	 */
	calculatePosition () {
		let h = this.oCloud.outerHeight(true);

		this.oParent.css({ paddingBottom: h });
	}

	/**
	 * listen for this.oBtn clicks and execute corresponding action
	 */
	handleButtonEvents () {
		let self = this,
			promise;

		this.oBtn.on('click', function () {
			let btn = $(this);

			try {
				promise = new Promise(function (resolve, reject) {

					if (btn.hasClass(self.o.btnCloseClass)) {
						resolve(self.collapseCloudContainer());
					} else {
						resolve(self.expandCloudContainer());
					}

				});

				promise
					.then(function () {
						self.calculatePosition();
					})
					.catch(function (reason) {
						if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(reason);
					});

			} catch (err) {
				new Error(err);
			}

		});
	}

	collapseCloudContainer () {
		let text = cbApp.Translator.translate('feed.tag_cloud.TK_tag_cloud_open');

		this.oBtn.removeClass(this.o.btnCloseClass).html(text + ' &raquo;');
		this.oCloud.removeClass(this.o.cloudOpenClass);

		this.calculatePosition();
	}

	expandCloudContainer () {
		let text = cbApp.Translator.translate('feed.tag_cloud.TK_tag_cloud_close');

		this.oBtn.addClass(this.o.btnCloseClass).html('&laquo; ' + text);
		this.oCloud.addClass(this.o.cloudOpenClass);

		this.calculatePosition();
	}

}
