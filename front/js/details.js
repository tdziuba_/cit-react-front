/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */

import * as c from './common';
import Gallery from './components/Gallery';
import DescriptionBox from './components/DescriptionBox';
import MapComponent from './components/MapComponent';
import ReportOffer from './components/ReportOffer';
import '../scss/details.scss';

$(document).ready( () => {

	c.executeCommon();

	cbApp.Gallery = new Gallery();
	cbApp.DescriptionBox = new DescriptionBox();
	cbApp.MapComponent = new MapComponent();
	cbApp.ReportOffer = new ReportOffer();

});
