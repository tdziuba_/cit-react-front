/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */
import '../scss/feed.scss';
import FBHelper from './components/FbHelper';
import * as c from './common';
import TagCloud from './components/TagCloud';
// import GridLayout from './components/GridLayout';
import Menu from './components/Menu.js';

$(document).ready( () => {
	// here will be code execution on document.readyState == 'complete'
	let fb = new FBHelper();

	$('#shareBtn').on('click', function () {
		fb.share(function () {
			alert('callback po wykonaniu share\'a');
		});
	});

	// if ($('.masonry-layout').length) {
	// 	window.cbApp.GridLayout = new GridLayout();
	// }

	if ($('.cb-section-top').find('.cntTree').length) {

		let categoryMenu = new Menu({
			menu: $('#categoryMenu')
		});

		let locationMenu = new Menu({
			menu: $('#locationMenu')
		});

		categoryMenu.addListener(locationMenu);
		locationMenu.addListener(categoryMenu);

	}

	c.executeCommon();

	if (typeof $.fn.dropdown == 'function') {
		$('.dropdown-toggle').dropdown();
	}

	new TagCloud();

});
