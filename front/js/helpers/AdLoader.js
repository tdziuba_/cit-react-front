/**
 * Created by Tomasz Dziuba on 20.01.2017.
 */
import postscribe from 'postscribe';

/**
 * parsing .ad elements html to find scripts and execute it by postscribe
 * usage:
 * <div class="ad">
 *     <script type="text/lazyad">
 *         <!--
 *         <script>alert('script executed by AdLoader')</script>
 *         -->
 *     </script>
 * </div>
 *
 * @constructor
 */
export default function AdLoader() {
	$('.ad').each((index, item) => {
		let parent = $(item).parent();
		let script = $(item).find('script[type$=lazyad]');
		let content = script.html().replace('<!--', '').replace('-->', '');

		try {
			postscribe(item, content, {
				autoFix: true,
				releaseAsync: true,
				done: () => {
					if (typeof console !== 'undefined' && typeof console.info !== 'undefined') console.info(parent.attr('id') + ' ad script has been delivered.');

					$(item).parents('.opxBox').removeClass('hidden');
				},
				error: (err) => {
					if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err.msg);
				},
				afterWrite: (obj) => {
					if (typeof console !== 'undefined' && typeof console.log !== 'undefined' && cbApp.appMode === 'development') console.log(obj);
				}
			});
		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}

	});
}
