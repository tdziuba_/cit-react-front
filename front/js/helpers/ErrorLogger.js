/**
 * Created by Tomasz Dziuba on 20.01.2017.
 */

export default class ErrorLogger {
	error;
	time;

	constructor (error) {
		this.error = this.stackTrace(error);
		this.time = this.getCurrentTime();

		this.log();
	}

	log () {
		let self = this;

		try {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined' && cbApp.appMode === 'development') console.warn(this.error.description, this.error);

			$.ajax({
				type: 'POST',
				url: '/error/js-error',
				data: JSON.stringify({
					time: self.time,
					userAgent: navigator.userAgent,
					details: self.error
				}),
				contentType: 'application/json; charset=utf-8'
			});

		} catch (e) {
			if (typeof console !== 'undefined') {
				console.warn(e);
			}
		}
	}

	getCurrentTime () {
		let c_date = new Date();
		let error_time = "Date: " + c_date.getDate() + "."
			+ (c_date.getMonth() + 1) + "."
			+ c_date.getFullYear() + " time: "
			+ c_date.getHours() + ":"
			+ c_date.getMinutes() + ":"
			+ c_date.getSeconds();

		return error_time;
	}

	stackTrace (error) {
		try {
			let stacktrace = {
				category: error.category,
				stack: error.stack,
				name: error.name,
				msg: error.message,
				file: (typeof error.sourceURL !== "undefined")? error.sourceURL: error.fileName,
				lineNumber: (typeof error.line !== "undefined")? error.line : error.lineNumber,
				number: error.number,
				description: (typeof error.description !== 'undefined')? error.description: error.toString(),
				location: document.location.href
			};

			return stacktrace;

		} catch (err) {

			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);

			return error;
		}

	}
}
