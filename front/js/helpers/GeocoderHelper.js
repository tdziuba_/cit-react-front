/**
 * Created by Tomasz Dziuba on 03.02.2017.
 */
import Cookies from 'js-cookie';

let instance;

export default class GeocoderHelper {

	coordinates;
	time;

	constructor () {
		if (!instance) {
			instance = this;
		}

		this.time = new Date();
		this.init();
	}

	getInstance () {
		return instance;
	}

	init () {

		if ( !this.coordinates && !Cookies.get('locationCoordinates') ) {
			this.tryToCalculateCoordinates();
		}

	}

	tryToCalculateCoordinates () {
		let self = this;

		if ("geolocation" in navigator) {

			try {

				navigator.geolocation.getCurrentPosition((position) => {
					self.setCoordinates({
						lat: position.coords.latitude,
						lng: position.coords.longitude
					});
				});

			} catch(err) {
				if (typeof console !== 'undefined' && typeof console.warn == 'function') console.warn(err);
			}

		}

	}

	setCoordinates (coordinates) {
		this.coordinates = coordinates;

		Cookies.set( 'locationCoordinates', JSON.stringify(this.coordinates) );
	}

	getCoordinates () {
		try {
			return (this.coordinates) ? this.coordinates : JSON.parse( Cookies.get('locationCoordinates') );
		} catch (err) {
			if (typeof console !== 'undefined' && typeof console.warn !== 'undefined') console.warn(err);
		}
	}
}
 