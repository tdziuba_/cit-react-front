/**
 * Created by Tomasz Dziuba on 19.01.2017.
 */
class Listeners {
	list = [];

	add (listener) {
		return this.list.push(listener);
	}

	remove (listener) {
		let index = this.findIndex(listener);
		this.removeByIndex(index);
	}

	count() {
		return this.list.length;
	}

	getByIndex (index) {
		if( index >= 0 && index < this.count() ) {
			return this.list[index];
		}
	}

	findIndex (obj) {
		let i = 0;

		while ( i < this.count() ) {
			if( this.getByIndex(i) === obj ){
				return i;
			}
			i++;
		}

		return -1;
	}

	removeByIndex (index) {
		if( index >= 0 && index < this.count() ) {
			this.list.splice(index, 1);
		}
	}
}

export default class Observer {
	listeners = new Listeners();

	constructor(oSender) {
		this.oSender = oSender;
	}

	add (listener) {
		this.listeners.add(listener);
	}

	remove (listener) {
		this.listeners.remove(listener);
	}

	notify (args) {
		for (let i = 0; i < this.listeners.count(); ++i) {
			this.listeners.getByIndex(i).update(this.oSender, args);
		}
	}
}
