/**
 * Created by Tomasz Dziuba on 02.01.2017.
 */

/**
 * Pojemnik na dane z serwera ustawiane podczas generowania html-a
 */
export class ServerData {
	_bCountersCheckServicesEnabled = false;
	_oRootLocation = null;
	_oSelectedLocation = null;

	_oSearchConstants = {
		INPUT_PARAM_NAME_LOCATION_ID: null,
		INPUT_PARAM_NAME_SEARCH_SCOPE: null,
		INPUT_PARAM_VALUE_SEARCH_SCOPE_BOARD_ITEMS: null,
		INPUT_PARAM_VALUE_SEARCH_SCOPE_YOUR_FRIENDS: null
	};

	Notifications = {
		_sListRefreshUrl: null,
		_sListCheckForNewBoardItemsServiceUrl: null,
		setListCheckForNewBoardItemsServiceUrl: function(sUrl) {
			this._sListCheckForNewBoardItemsServiceUrl = sUrl;
		},
		getListCheckForNewBoardItemsServiceUrl: function() {
			return this._sListCheckForNewBoardItemsServiceUrl;
		},
		setListRefreshUrl: function(sUrl) {
			this._sListRefreshUrl = sUrl;
		},
		getListRefreshUrl: function() {
			return this._sListRefreshUrl;
		}

	};

	Facebook = {
		_sLoginUrl: null,
		setLoginUrl: function(sUrl) {
			this._sLoginUrl = sUrl;
		},
		getLoginUrl: function() {
			return this._sLoginUrl;
		}
	};

	PersonProfile = {
		_isPersonProfilePage: false,
			setPersonProfilePage: function(bVal) {
			this._isPersonProfilePage = bVal;
		},
		isPersonProfilePage: function() {
			return this._isPersonProfilePage;
		}
	};

	TranslateKeyContainers = {
		_oSearchControl: null,
		_oSuggesterControl: null,
		_oBoardItemSubscriptionAction: null,
		_oCommonElements: null,
		_oUserPanelItems: null,
		/**
		 * @param {TranslateKeyContainer} oTranslateKeyContainer
		 */
		setForCommonElements: function(oTranslateKeyContainer) {
			this._oCommonElements = oTranslateKeyContainer;
		},
		/**
		 * @return {TranslateKeyContainer} oTranslateKeyContainer
		 */
		getForCommonElements: function() {
			return this._oCommonElements;
		},
		/**
		 * @param {TranslateKeyContainer} oTranslateKeyContainer
		 */
		setForUserPanelItems: function(oTranslateKeyContainer) {
			this._oUserPanelItems = oTranslateKeyContainer;
		},
		/**
		 * @return {TranslateKeyContainer} oTranslateKeyContainer
		 */
		getForUserPanelItems: function() {
			return this._oUserPanelItems;
		},
		/**
		 * @param {TranslateKeyContainer} oTranslateKeyContainer
		 */
		setForSearchControl: function(oTranslateKeyContainer) {
			this._oSearchControl = oTranslateKeyContainer;
		},
		/**
		 * @return {TranslateKeyContainer} oTranslateKeyContainer
		 */
		getForSearchControl: function() {
			return this._oSearchControl;
		},
		/**
		 * @param {TranslateKeyContainer} oTranslateKeyContainer
		 */
		setForBoardItemSubscriptionAction: function(oTranslateKeyContainer) {
			this._oBoardItemSubscriptionAction = oTranslateKeyContainer;
		},
		/**
		 * @return {TranslateKeyContainer} oTranslateKeyContainer
		 */
		getForBoardItemSubscriptionAction: function() {
			return this._oBoardItemSubscriptionAction;
		},
		/**
		 * @param {TranslateKeyContainer} oTranslateKeyContainer
		 */
		setForSuggesterControl: function(oTranslateKeyContainer) {
			this._oSuggesterControl = oTranslateKeyContainer;
		},
		/**
		 * @return {TranslateKeyContainer} oTranslateKeyContainer
		 */
		getForSuggesterControl: function() {
			return this._oSuggesterControl;
		}
	};

	constructor() {

	}

	setCountersCheckServicesEnabled(bVal) {
		this._bCountersCheckServicesEnabled = bVal;
	}

	isCountersCheckServicesEnabled() {
		return this._bCountersCheckServicesEnabled;
	}

	/**
	 * @param {LocationModel} oLocation
	 */
	setRootLocation (oLocation) {
		this._oRootLocation = oLocation;
	}

	/**
	 * @returns {LocationModel}
	 */
	getRootLocation () {
		return this._oRootLocation;
	}

	/**
	 * @param {LocationModel} oLocation
	 */
	setSelectedLocation (oLocation) {
		this._oSelectedLocation = oLocation;
	}

	/**
	 * @returns {LocationModel}
	 */
	getSelectedLocation () {
		return this._oSelectedLocation;
	}

	/**
	 * @returns {Boolean}
	 */
	hasSelectedLocation () {
		return (this._oSelectedLocation !== null);
	}

	setSearchConstants (oSearchConstants) {
		$.extend(this._oSearchConstants, oSearchConstants);
	}

	/**
	 * @returns {___anonymous4206_4393}
	 */
	getSearchConstants () {
		return this._oSearchConstants;
	}

}

export class TranslateKeyContainer {
	constructor(oKeys) {
		if (oKeys) {
			for (sKeyName in oKeys) {
				this[sKeyName] = oKeys[sKeyName];
			}
		}
	}

	setValueFor(sKeyName, sValue) {
		this[sKeyName] = sValue;

 		return this;
	}

	getValueFor (sKeyName) {
		if (this.hasOwnProperty(sKeyName)) {
			return this[sKeyName];
		}
		else {
			return sKeyName;
		}
	}

}
