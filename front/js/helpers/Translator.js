/**
 * Created by Tomasz Dziuba on 04.01.2017.
 */
import i18next from 'i18next';
import * as keys from '../../../dist/locales/se_translation-keys.json'

export default class Translator {

	options = {};
	keys = keys;
	translator;

	constructor(options) {
		let self = this;

		this.options = $.extend({}, {
			pattern: /TK_([a-zA-Z_]+)/gm,
			selector: '.translation',
			language: $('html').attr('lang')
		}, options);

		i18next.init({
			lng: 'se',
			resources: {
				se: {
					translation: self.keys
				}
			}
		}, () => {
			self.setTranslator( i18next );
		});

	}

	setTranslator (translator) {
		this.translator = translator;
	}

	parse() {
		let self = this,
			selector = $(this.options.selector);

		$.each(selector, function () {
			self.replaceText($(this));
		});
	}

	replaceText(element, t) {
		let text = (typeof t !== 'undefined') ? t : element.text().trim(),
			newText = this.translator.t(text);

		element.text(newText);
	}

	translate (key) {
		return this.translator.t(key);
	}
}
