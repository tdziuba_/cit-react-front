/**
 * Created by Tomasz Dziuba on 24.01.2017.
 */

import path from 'path';

let outputFileName = path.resolve('./dist/details.html');

export function renderForDetails(tpl, req, res) {
	let data = tpl.renderCommon();
	let md = tpl.md;
	let isMobile = (md.mobile() && !md.tablet());

	data.breadcrumbs = tpl.renderTemplate('components/details/breadcrumbs.hbs', {});
	data.detailsHeader = tpl.renderTemplate('components/details/details-header.hbs', { isMobile: isMobile });
	data.detailsGallery = tpl.renderTemplate('components/details/gallery.hbs', { isMobile: isMobile });
	data.contactWrapper = tpl.renderTemplate('components/details/details-contact-box.hbs', {});
	data.descriptionWrapper = tpl.renderTemplate('components/details/description.hbs', {});
	data.favoritesWrapper = tpl.renderTemplate('components/details/favorites-pommes-frites.hbs', { isMobile: isMobile });
	data.actionsWrapper = tpl.renderTemplate('components/details/offer-actions.hbs', { isMobile: isMobile });
	data.locationWrapper = tpl.renderTemplate('components/details/location-section.hbs', { isMobile: isMobile });
	data.reportWrapper = tpl.renderTemplate('components/details/report-offer.hbs', { isMobile: isMobile });
	data.isMobile = isMobile;

	tpl.renderLayout(res, {
		html: tpl.getInstance().renderTemplate('details.hbs', data),
		script: 'details',
		showGridScript: false,
		md: tpl.md
	}, outputFileName);
}