/**
 * Created by Tomasz Dziuba on 24.01.2017.
 */

import path from 'path';

let outputFileName = path.resolve('./dist/index.html');

export function renderForFeed(tpl, req, res) {
	let data = tpl.renderCommon();

	data.location = (req.params['loc2']) ? req.params['loc2'] : req.params['loc1'];

	if (req.params[1]) {
		data.category = req.params['loc3'];
	}

	if (req.query) {
		data.inp_text = req.query['inp_text'];
	}
;
	data.script = 'feed';
	data.feed = tpl.renderTemplate('components/feed-list.hbs', {
		side_menu: tpl.renderTemplate('components/sidebar.hbs', {md: tpl.md})
	});
	data.sectionTop = tpl.renderTemplate('components/top/top-section.hbs', {
		topBanner: tpl.renderTemplate('components/top/top-banner.hbs', {md: tpl.md}),
		categories: tpl.renderTemplate('components/top/categories.hbs', {md: tpl.md}),
		locations: tpl.renderTemplate('components/top/locations.hbs', {md: tpl.md}),
		selectedLocation: data.location,
		selectedCategory: data.category,
		searchedText: data.inp_text,
		showSelectedWrapper: (data.inp_text || data.selectedCategory || data.selectedLocation)
	});
	data.gridItemTpl = tpl.renderTemplate('js-tpl/grid-item-tpl.hbs');
	data.tagCloud = tpl.renderTemplate('components/tag-cloud.hbs', {md: tpl.md});
	data.listSort = tpl.renderTemplate('components/list-sort.hbs', {md: tpl.md});
	data.shopelloTpl = tpl.renderTemplate('js-tpl/shopello-item-tpl.hbs', {category: 331});

	data.html = tpl.renderTemplate('feed.hbs', data);

	tpl.renderLayout(res, {
		html: data.html,
		script: 'feed',
		showGridScript: true,
		md: tpl.md
	}, outputFileName);
}