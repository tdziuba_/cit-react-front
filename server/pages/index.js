/**
 * Created by Tomasz Dziuba on 24.01.2017.
 */
import { renderForFeed } from './feed';
import { renderForDetails } from './details';
import { renderForProfile } from './profile';

export {
	renderForFeed,
	renderForDetails,
	renderForProfile
}
