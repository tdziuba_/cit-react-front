/**
 * Created by Tomasz Dziuba on 27.01.2017.
 */

export function renderForProfile(tpl, req, res) {
	let data = tpl.renderCommon();

	tpl.renderLayout(res, {
		html: tpl.renderTemplate('profile.hbs', data),
		script: 'profile',
		showGridScript: false,
		md: tpl.md
	});
}
