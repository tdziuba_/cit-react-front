/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */

import path from 'path';
import fs from 'fs';
import handlebars from 'handlebars';
import Express from "express";
import helmet from 'helmet';
import compression from 'compression';
import MobileDetect from 'mobile-detect';
import TemplateHelper from './template-helper';
import * as p from './pages';

let app = Express();
let tpl = new TemplateHelper();
const port = 3000;

app.use(compression());
app.use(helmet());
app.disable('x-powered-by');
app.use( '/assets', Express.static( path.resolve(__dirname + '/../dist') ) );

app.set('view engine', 'hbs');

app.post('/error/js-error', (req, res, next) => {
	res.json(JSON.stringify({...req.params}));
});

app.get('/load-more', (req, res, next) => {
	res.json(tpl.renderJSON('feed'+req.query['offset']+'.json'));
});

app.get('/board/shopello-list-offers', (req, res, next) => {
	res.json(tpl.renderJSON('shopello'+req.query.category+'.json'));
});

app.get('/:title,:id', (req, res, next) => {
	let md = new MobileDetect(req.headers['user-agent']);
	p.renderForDetails(tpl.getInstance(md), req, res);
});

app.get('/personen-:uid', (req, res, next) => {
	let md = new MobileDetect(req.headers['user-agent']);
	p.renderForProfile(tpl.getInstance(md), req, res);
});

app.get('/contact', (req, res, next) => {
// 	let data = renderCommon();
// 	let md = new MobileDetect(req.headers['user-agent']);
//
// 	renderLayout(res, {
// 		html: renderTemplate('./tpl/contact.hbs', data),
// 		script: 'customer-service',
// 		showGridScript: false,
// 		md: md
// 	});
});



app.get('/:location', (req, res) => {
	let md = new MobileDetect(req.headers['user-agent']);

	p.renderForFeed(tpl.getInstance(md), req, res);
});

app.get('/:loc1/:loc2', (req, res) => {
	let md = new MobileDetect(req.headers['user-agent']);

	p.renderForFeed(tpl.getInstance(md), req, res);
});

app.get('/:loc1/:loc2/:loc3', (req, res) => {
	let md = new MobileDetect(req.headers['user-agent']);

	p.renderForFeed(tpl.getInstance(md), req, res);
});

app.get('/', (req, res) => {
	let md = new MobileDetect(req.headers['user-agent']);

	p.renderForFeed(tpl.getInstance(md), req, res);
});

handlebars.registerHelper('getMobileClass', (md) => {
	let cssClass = '';

	if (md.mobile() !== null) {
		cssClass += ' isMobile';
	}

	if (md.is('iPhone') || md.is('iPad') || md.os() === 'iOS') {
		cssClass += ' ios';
	}

	if (md.os() === 'AndroidOS') {
		cssClass += ' android';
	}

	return cssClass;
});

handlebars.registerHelper('ifAndroid', (md, options) => {
	if (md.os() === 'AndroidOS' && (md.mobile() !== null || md.tablet() !== null)) {
		return options.fn(this);
	}
});

handlebars.registerHelper('ifiOS', (md, options) => {
	if (md.os() === 'iOS' && (md.mobile() !== null || md.tablet() !== null)) {
		return options.fn(this);
	}
});

app.listen(port);
