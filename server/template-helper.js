/**
 * Created by Tomasz Dziuba on 24.01.2017.
 */
import handlebars from 'handlebars';
import path from 'path';
import fs from 'fs';

let instance;

export default class TemplateHelper {
	/**
	 * @param {MobileDetect} md
	 */
	md;
	constructor() {
		if(!instance){
			instance = this;
		}

		// this.registerHelpers();

		return instance;
	}

	getInstance(md) {
		instance.md = md;

		return instance;
	}

	registerHelpers() {
		let self = this;

		handlebars.registerHelper('getMobileClass', (md) => {
			let cssClass = '';

			if (self.md.mobile() !== null) {
				cssClass += ' isMobile';
			}

			if (self.md.is('iPhone') || md.is('iPad') || md.os() === 'iOS') {
				cssClass += ' ios';
			}

			if (self.md.os() === 'AndroidOS') {
				cssClass += ' android';
			}

			return cssClass;
		});

		handlebars.registerHelper('ifAndroid', (md, options) => {
			if (self.md.os() === 'AndroidOS' && (md.mobile() !== null || md.tablet() !== null)) {
				return options.fn(this);
			}
		});

		handlebars.registerHelper('ifiOS', (md, options) => {
			if (self.md.os() === 'iOS' && (self.md.mobile() !== null || self.md.tablet() !== null)) {
				return options.fn(this);
			}
		});
	}

	renderTemplate(fileName, data) {
		let tpl = require(path.resolve('./server/tpl/' + fileName));

		return tpl({ data: data }).replace(/\{\-\{/g, '{{');

	}

	includeJsTemplate(fileName) {
		let tpl = require(path.resolve('./server/tpl/js-tpl/' + fileName));

		return tpl().replace(/\{\-\{/g, '{{');
	}

	renderJSON(fileName) {
		return require(path.resolve('./server/jsons/' + fileName));
	}

	renderCommon() {
		let header = this.renderTemplate('components/header.hbs', {
			sideMenu: this.renderTemplate('components/side-menu.hbs', {md: this.md}),
			md: this.md
		});
		let footer = this.renderTemplate('components/footer.hbs', {md: this.md});

		return {
			header: header,
			footer: footer,
			md: this.md
		}
	}

	renderLayout(res, data, fileName) {
		let layout = this.renderTemplate('index.hbs', {
			html: data.html,
			gridItemTpl: this.renderTemplate('js-tpl/grid-item-tpl.hbs', {}),
			script: data.script,
			gridScript: (data.showGridScript) ? this.renderTemplate('components/grid-script.hbs', {}) : null,
			modalTpl: this.includeJsTemplate('modal-general-tpl.hbs'),
			ads: this.renderTemplate('components/ads-scripts.hbs', {}),
			oneSignal: this.renderTemplate('components/one-signal.hbs', {})
		} );

		if (fileName) {
			fs.writeFile(fileName, layout, function (err) {
				if (err) return console.log(err);
			});
		}

		return res.send( layout );
	}
}
