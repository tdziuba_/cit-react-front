/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */
var path = require('path')
var webpack = require('webpack'),
	// ProgressBarPlugin = require('progress-bar-webpack-plugin'),
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	sassLintPlugin = require('sasslint-webpack-plugin'),
	sassLoaderPaths = [path.resolve(__dirname, "./scss"), path.resolve(__dirname, "./node_modules/bootstrap/scss")];

//sassLoaderPaths = sassLoaderPaths.concat(require('node-neat').includePaths);

module.exports = {
	devtool: 'eval-source-map',
	debug: true,
	watch: true,
	aggregateTimeout: 300,
	poll: 1000,
	target: 'node',
	entry: {
		feed: ['./front/js/feed.js'],
		details: ['./front/js/details.js'],
		customerService: ['./front/js/customer-service.js'],
		profile: ['./front/js/profile.js'],
		grid: [
			'./front/js/vendor/masonry.pkgd.js',
			'./front/js/vendor/imagesloaded.pkgd.js',
			'./front/js/components/LoadMore.js',
			'./front/js/components/ShopelloItems.js',
			'./front/js/components/GridLayout.js'
		],
		vendor: [
			'./front/js/vendor/jquery.slim.js',
			'tether',
			'bootstrap',
			//'./front/js/vendor/tether.js',
			//'./front/js/vendor/bootstrap.js',
			'./front/js/vendor/hammer.js',
			'./front/js/vendor/hammer-time.min.js',
			'./front/js/vendor/handlebars.amd.js'
		]
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'js/cbApp.[name].js',
		chunkFilename: 'js/cbApp.[id].chunk.js',
		publicPath: '/',
		target: 'browser',
		library: ["cbApp", "[name]"],
		libraryTarget: "umd"
	},

	externals: {
		jQuery: 'jQuery',
		Tether: 'Tether',
		cbApp: 'cbApp'
	},

	sassLoader: {
		includePaths: sassLoaderPaths
	},
	eslint: {
		configFile: './.eslintrc'
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('development')
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			'Tether': 'tether',
			'window.Tether': 'tether'
		}),

		// new sassLintPlugin({
		// 	configFile: '.sass-lint.yml',
		// 	ignoreFiles: [],
		// 	ignorePlugins: ['extract-text-webpack-plugin'],
		// 	glob: './front/**/*.scss',
		// 	quiet: false,
		// 	failOnWarning: false,
		// 	failOnError: false,
		// 	testing: false
		// }),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			children: true,
			// (use all children of the chunk)

			async: true,
			// (create an async commons chunk)
			minChunks: 2
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "commons",
			chunks: [
				"vendor",
				"./front/js/common.js"
			],
			children: true,
			async: true,
			minChunks: Infinity
		}),
		new ExtractTextPlugin("css/[name].css", {
			allChunks: true,
			disable: false
		}),
	],
	resolveLoader: {

	},
	resolve: {
		extensions: ["", ".webpack.js", ".js", ".scss"],
		root: __dirname,
		alias: {
			Handlebars: 'handlebars/dist/handlebars.min.js',
			'handlebars' : 'handlebars/dist/handlebars.js',
			'Tether': 'tether/dist/js/tether.js'
		}
	},
	module: {
		preLoaders: [
			//{ test: /\.js?$/, loader: 'eslint-loader', exclude: /node_modules/ },
			{ test: /\.js$/, loader: "source-map-loader" },
		],
		loaders: [
			//{ test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery&Tether=tether' },
			{ test: /\.js?$/, loader: 'babel-loader', exclude: /node_modules/ },
			{ test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css?sourceMap&-url!sass-loader?sourceMap") },
			{ test: /\.json$/, loader: "json-loader" },

			{ test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
			{ test: /\.ttf$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.eot$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.svg$/,  loader: "file-loader?prefix=font/" },

			//img
			{ test: /\.(png|gif|jpg)$/, loader: 'url-loader?name=[path][name].[ext]' }
		]
	}

}
