/**
 * Created by Tomasz Dziuba on 28.12.2016.
 */
var path = require('path')
var webpack = require('webpack'),
	// ProgressBarPlugin = require('progress-bar-webpack-plugin'),
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	CompressionPlugin = require("compression-webpack-plugin"),
	sassLintPlugin = require('sasslint-webpack-plugin'),
	sassLoaderPaths = [path.resolve(__dirname, "./front/scss")];

//sassLoaderPaths = sassLoaderPaths.concat(require('node-neat').includePaths);

module.exports = {
	devtool: 'cheap-module-source-map',
	debug: false,
	watch: false,
	entry: {
		feed: ['./front/js/feed.js'],
		details: ['./front/js/details.js'],
		customerService: ['./front/js/customer-service.js'],
		profile: ['./front/js/profile.js'],
		grid: [
			'./front/js/vendor/masonry.pkgd.js',
			'./front/js/vendor/imagesloaded.pkgd.js',
			'./front/js/components/LoadMore.js',
			'./front/js/components/ShopelloItems.js',
			'./front/js/components/GridLayout.js'
		],
		vendor: [
			'./front/js/vendor/jquery.slim.js',
			'./front/js/vendor/tether.js',
			'./front/js/vendor/bootstrap.js',
			'./front/js/vendor/hammer.js',
			'./front/js/vendor/hammer-time.min.js',
			'./front/js/vendor/handlebars.amd.js'
		]
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'js/[name].js',
		chunkFilename: 'js/[name].chunk.js',
		publicPath: 'http://localhost:3000/',
		library: 'cbApp',
		libraryTarget: 'var'
	},

	externals: {
		jQuery: 'jQuery',
	},

	sassLoader: {
		includePaths: sassLoaderPaths
	},
	eslint: {
		configFile: './.eslintrc'
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
		// new ProgressBarPlugin(),
		//new sassLintPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: "commons",
			chunks: ["vendor", "./front/js/common.js", "grid"],
			minChunks: 2
		}),
		new webpack.optimize.LimitChunkCountPlugin({maxChunks: 10}),
		new webpack.optimize.MinChunkSizePlugin({minChunkSize: 5000}),
		new webpack.optimize.AggressiveMergingPlugin({
			minSizeReduce: 1.5,
			moveToParents: true
		}),
		new ExtractTextPlugin("css/[name].css", {
			allChunks: true,
			disable: false
		}),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$|\.html$/,
			threshold: 10240,
			minRatio: 0.8
		})
	],
	resolveLoader: {

	},
	resolve: {
		extensions: ["", ".webpack.js", ".js", ".hbs", ".scss"],
		alias: {
			Handlebars: 'handlebars/dist/handlebars.min.js',
			'handlebars' : 'handlebars/dist/handlebars.js'
		}
	},
	module: {
		preLoaders: [
			{ test: /\.js?$/, loader: 'eslint-loader', exclude: ['node_modules', './front/js/vendor'] },
			{ test: /\.js$/, loader: "source-map-loader" },
		],
		loaders: [
			{ test: /\.js?$/, loader: 'babel-loader', exclude: /node_modules/ },
			{ test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css?sourceMap&-url!sass-loader?sourceMap") },
			{
				test: /\.json$/,
				loaders: [ 'json' ],
				exclude: /node_modules/,
				include: __dirname
			},

			{ test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
			{ test: /\.ttf$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.eot$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.svg$/,  loader: "file-loader?prefix=font/" },

			//img
			{ test: /\.(png|gif|jpg)$/, loader: 'url-loader?name=[path][name].[ext]' }
		]
	}

}
